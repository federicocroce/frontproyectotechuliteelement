# Imagen base
FROM node:latest

WORKDIR app

# Copiado de archivos
COPY ./dist app

# Puerto que expongo
EXPOSE 3001

#Comandos
CMD ["npm", "run", "start"]
