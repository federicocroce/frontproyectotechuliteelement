import methodsServices from "../services";
import {getMortgageLoans} from '../../actions/actionMortgageLoans';

const { get, post, remove, put, setSocketIo, test } = methodsServices;

const route = "mortgageLoans";

const controller = {};

let invokedSocket = false;

const cbkIO = (response) => {
  response && getMortgageLoans(response);
}

const socketPath = () =>{
  const id = sessionStorage.email && sessionStorage.email 
  return `${route}-${id}`
}

controller.invokedSocket = (state = false) => invokedSocket = state;

controller.test = () => {
  test(route);
};



controller.get = () => get(route);

controller.getDocument = id => {  
  if(!invokedSocket){
    setSocketIo(socketPath(), cbkIO);
    invokedSocket = true;
  } 
    
  return get(`${route}/data`);
};
controller.post = document => post(route, document);
controller.remove = id => remove(`${route}/${id}`);
controller.put = (id, document) => put(route, id, document);

export default controller;
