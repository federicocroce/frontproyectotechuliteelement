import { store } from "../../config/store";
import methodsServices from "../services";
import {
  getAdvancedInstallments,
  testActionMort
} from "../../actions/actionAdvancedInstallments";

const { get, getRedux, post, remove, put, setSocketIo, test } = methodsServices;

const route = "advancedInstallments";

const controller = {};

let invokedSocket = false;

const cbkIO = response => {
  response && getAdvancedInstallments(response);
};

const socketPath = () =>{
  const id = sessionStorage.email && sessionStorage.email 
  return `${route}-${id}`
}

controller.invokedSocket = (state = false) => invokedSocket = state;

controller.test = () => {
  test(route);
};
controller.get = () => get(route);

controller.getDocument = id => {
  if (!invokedSocket) {
    setSocketIo(socketPath(), cbkIO);
    invokedSocket = true;
  }

  const storeA = store.getState();

  return getRedux(
    `${route}/${storeA.reducerMortgageLoans.mortgageSelected._id}`,
    getAdvancedInstallments
  );
};

controller.post = document => post(`${route}`, document);
controller.remove = (id, _idMortgageLoans) =>
  remove(`${route}/${id}/${_idMortgageLoans}`);
controller.put = (id, document) => put(route, id, document);

export default controller;
