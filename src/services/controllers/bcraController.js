// import { get, post, remove, put, test } from "../services";
import methodsServices from "../services";

const { get, post, remove, put, test } = methodsServices;

const controller = {};

controller.test = () => {
  test(route);
};
controller.get = (cbkIO, route) => get(route, cbkIO);

controller.getDocument = ({route, date})=> {
  return get(date ? `${route}?date=${date}`: `${route}`)
};

controller.post = document => post(route, document);
controller.remove = id => remove(route, id);
controller.put = (id, document) => put(route, id, document);

export default controller;
