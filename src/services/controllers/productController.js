import methodsServices from "../services";

const { get, post, remove, put, test } = methodsServices;

const route = "product";

const controller = {};

controller.test = () => {
  test(route);
};
controller.get = (cbkIO) => get(route, cbkIO);
controller.getDocument = id => get(`${route}/${id}`);
controller.post = document => post(route, document);
controller.remove = id => remove(route, id);
controller.put = (id, document) => put(route, id, document);

export default controller;
