import methodsServices from "../services";

const { get, post, test } = methodsServices;
const headers = { 
  "Content-Type": "application/json; charset=utf-8"
}

const controller = {};

controller.test = () => {
  test(route);
};

controller.testPrivate = () => get("private");
controller.signIn = document => post("signin", document, headers);
controller.signUp = document => post("signup", document, headers);

export default controller;
