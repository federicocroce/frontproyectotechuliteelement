import socketIOClient from "socket.io-client";
import axios from "axios";

const hostname = window.location.hostname == "localhost" ? "localhost" : '54.166.151.221';

const path = `http://${hostname}:3000`;

const socket = socketIOClient(path);

const methodsServices = {};

const baseHeaders = {
  "Accept": "application/json",
  "Content-Type": "application/json; charset=utf-8",
};

const setauthorization = () => {
  if (sessionStorage.token && sessionStorage.token != "undefined")
   return {...baseHeaders , 'Authorization': `Beare ${sessionStorage.token}`};
   return baseHeaders;
};

const sleep = (ms, nombre) => new Promise(res => setTimeout(res, ms));

methodsServices.setSocketIo = (socketPath, cbkIO) => {
  return socket.on(`${socketPath}`, data => cbkIO(data))
}

methodsServices.get = (servicePath, cbkIO, headers = setauthorization()) => {

  return callService(
    axios({
      method: "get",
      headers,
      url: `${path}/${servicePath}`,
    })
  );
};

methodsServices.getRedux = (servicePath, action, headers = setauthorization()) => {

  return callServiceReudx(
    axios({
      method: "get",
      headers,
      url: `${path}/${servicePath}`,
    }), action
  );
};

methodsServices.post = (servicePath, document, headers = setauthorization()) => {
  return callService(
 
    axios({
      method: "post",
      headers,
      url: `${path}/${servicePath}`,
      data: document
    })

  );
};

methodsServices.put = (servicePath, id, document, headers = setauthorization()) => {
  return callService( 
    axios({
      method: "PUT",
      headers,
      url: `${path}/${servicePath}/${id}`,
      data: document
    })
  );
};


methodsServices.remove = (servicePath, headers = setauthorization()) => {
  return callService(
    axios({
      method: "DELETE",
      headers,
      url: `${path}/${servicePath}`
    })
  );
};

const callService = (call) =>
  (async () => {
    return await call;
  })();

  const callServiceReudx = (call, action) =>
  (async () => {
    const response = await call;
    action(response.data.result)
    return response;
  })();

export default methodsServices;
