import { LitElement, html, css } from "lit-element";

class LitTransition extends LitElement {
  static get styles() {
    return css`
      /* 
      section{
          opacity: 0;
      } */
    `;
  }

  static get properties() {
    return {
      section: { type: Function },
      duration: {type: Number},
    };
  }

  constructor() {
    super();

    this.duration = "1";
  }

  firstUpdated() {
    this.setAttribute('style', `transition-duration:${this.duration}s;` )
    setTimeout(() => {
      this.classList.add("transition");
    }, 1);
  }

  render() {
    return html`
      <style>
        :host {
          display: block;
          opacity: 0;
          transition: ${`all ${this.duration}s ease-in-out`};
          transform: translateY(20px);
        }

        :host(.transition) {
          opacity: 1;
          transform: translateX(0);
        }
      </style>
      <slot></slot>
    `;
  }

  // createRenderRoot() {
  //   return this;
  // }
}

customElements.define("lit-transition", LitTransition);
