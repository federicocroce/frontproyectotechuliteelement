import { LitElement, html, css } from "lit-element";
import "@polymer/paper-spinner/paper-spinner.js";

class ManageServicesCall extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        width: 100%;
      }

      paper-spinner {
        margin: 0 auto;
        position: absolute;
        left: 0;
        right: 0;
        top: 50%;
        transform: translateY(-50%);
      }
    `;
  }

  static get properties() {
    return {
      renderComponent: { type: Function },
      name: { type: String },
      successComponent: { type: Function },
      loadingComponent: { type: Function },
      errorComponent: { type: Function },
      errorResponse: { type: String },
      successResponse: { type: Object },
      callbackSuccess: { type: Function },
      callbackError: { type: Function },
      isLoading: { type: Boolean },
      hasError: { type: Boolean },
      service: { type: Function },
      rerender: { type: Array }
    };
  }

  constructor() {
    super();

    this.isLoading = false;
    this.hasError = false;
    this.successResponse = null;

    this.loadingComponent = () =>
      html`
        <paper-spinner active></paper-spinner>
      `;
  }

  connectedCallback() {
    super.connectedCallback();
    this.setDataProvider();
  }

  updated(changedProperties) {
    changedProperties.forEach((oldValue, propName) => {
      propName == "service" &&
        this.service &&
        oldValue != this.service &&
        this.setDataProvider();
    });
  }

  async setDataProvider() {
    if (!this.service) return;

    const { name, callbackSuccess, callbackError, service } = this;

    this.isLoading = true;

    const thenResponse = ({ data }) => {
      this.hasError = false;
      if (data) this.successResponse = data;
      if (callbackSuccess) callbackSuccess(data);
    };

    const thenResponseArray = data => {
      this.hasError = false;
      if (data) this.successResponse = data;
      if (callbackSuccess) callbackSuccess(data);
    };

    const catchResponse = error => {
      const { response } = error;
      console.log("Esto es una error: " + response.data.message);
      this.hasError = true;
      if (response) this.errorResponse = response;
      if (callbackError) callbackError(response.data, error);
    };

    const finallyResponse = () => (this.isLoading = false);

    if (Array.isArray(service)) {
      await Promise.all(service)
        .then(thenResponseArray)
        .catch(catchResponse)
        .finally(finallyResponse);
    } else {
      await service
        .then(thenResponse)
        .catch(catchResponse)
        .finally(finallyResponse);
    }
  }

  render() {
    this.renderComponent = null;

    if (this.loadingComponent && this.isLoading) {
      this.renderComponent = this.loadingComponent();
    } else if (this.hasError && this.errorComponent && !this.isLoading) {
      this.renderComponent = this.errorComponent(this.errorResponse.data, this.errorResponse);
    } else if (
      !this.hasError &&
      !this.isLoading &&
      this.successComponent &&
      this.successResponse
    ) {
      this.renderComponent = this.successComponent(this.successResponse);
    }

    return html`
      ${this.renderComponent}
    `;
  }

  // createRenderRoot() {
  //   return this;
  // }
}

customElements.define("manage-services-call", ManageServicesCall);
