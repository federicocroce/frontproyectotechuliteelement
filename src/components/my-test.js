import { LitElement, html, css } from 'lit-element';

class Test  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
        name:{type: String},
        cbk: {type: Function},
        obj: {type: Function}
    };
  }

  constructor() {
    super();
  }

  render() {
    return html`
       <p>${this.name}</p>
       <button @click="${this.click}">Esto es un boton js</button>
       <button @click="${() => this.cbk("soy un param")}">CBK</button>

       ${this.obj()}
    `;
  }

  click(){
    alert("Hice click");
  }
}

customElements.define('my-test', Test);