import { LitElement, html, css } from 'lit-element';

class InputText  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
        name: {type: String},
        value: {type: String},
        type: {type: String},
        onChange: {type: Function}
        
    };
  }

  constructor() {
    super();
  }

  render() {
    return html`
         <input
          name="username"
          type="text"
          value="${this.inputs.username}"
          @input="${(e) => this.onChange(e.target)}"
        />
    `;
  }
}

customElements.define('input-text', InputText);