import { LitElement, html, css } from "lit-element";

class KeyValue extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }


  static get properties() {
    return {
      key: { type: String },
      value: { type: String }
    };
  }

  constructor() {
    super();
  }

  render() {
    return html`
      <style>
        .key {
          font-size: 12px;
          color: gray;
          font-weight: bold;
        }

        .value {
          color: #157f90;
          font-weight: bold;
          font-size: 20px;
        }
        p {
          margin: 0;
          display: inline-block;
        }
      </style>

      <p class="key">${this.key}</p>
      <p class="value">${this.value}</p>
    `;
  }

  createRenderRoot() {
    return this;
  }
}

customElements.define("key-value", KeyValue);
