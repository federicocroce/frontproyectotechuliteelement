import { LitElement, html, css } from "lit-element";

import "@polymer/paper-input/paper-input.js";

class InputMonthYear extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        width: 100%;
        position: relative;
        padding-bottom: 30px;
      }
      .year {
        font-size: 12px;
        position: absolute;
        bottom: 12px;
      }
    `;
  }

  static get properties() {
    return {
      name: { type: String },
      label: { type: String },
      value: { type: Number },
      input: { type: Function },
      year: { type: String },
      required: { type: Boolean },
      autoValidate: { type: Boolean },
    };
  }

  constructor() {
    super();
    this.required = false;
  }

  validate = () =>{
    return this.shadowRoot.querySelector(`[name='${this.name}']`).validate()
  }


  render() {
    return html`
      <style>
        paper-input {
          --paper-input-container: {
            padding-bottom: 0 !important;
          }
        }
      </style>

      <paper-input
        name="${this.name}"
        label="${this.label}"
        type="number"
        value="${this.value || ""}"
        @input="${this.input}"
        ?required="${this.required}"
        auto-validate="${this.autoValidate}"
        error-message="Este campo es obligatorio"
      >
        <div slot="suffix">meses</div>
      </paper-input>

      <span class="year">${this.setYear()}</span>
    `;
  }

  setYear() {
    if (!this.value) return;
    const year = Math.round((parseInt(this.value) / 12) * 100) / 100;
    return year >= 1 ? `${year} ${year == 1 ? "AÑO" : "AÑOS"}` || "" : "";
  }
}

customElements.define("input-month-year", InputMonthYear);
