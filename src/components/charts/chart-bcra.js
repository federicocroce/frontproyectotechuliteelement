import { LitElement, html, css } from "lit-element";

import highcharts from "highcharts";
import "highcharts/modules/exporting";
import "highcharts/modules/boost";
import "highcharts/highcharts-more";

class LdChart extends LitElement {

  static get properties() {
    return {
      title:{type: String},
      series: { type: Array },
      categories: { type: Array },
      typeChart: {type: String}
    };
  }

  static get styles() {
    return css`
      :host {
        display: block;
        width: 100%;
        height: 50%;
      }
    `;
  }

  firstUpdated(changedProperties) {
    this._enableChart();
  }

  updated(changedProperties) {
    changedProperties.forEach((oldValue, propName) => {
      propName == "typeChart" && this.typeChart && oldValue != this.typeChart && this._enableChart();
    });
  }

  render() {
    return html`
      <div
        id="container"
      ></div>
    `;
  }

  _enableChart() {
    highcharts.chart(this.shadowRoot.querySelector("#container"), {
      chart: {
        type: this.typeChart
      },
      title: {
        text: this.title
      },
      xAxis: {
        categories: this.categories
      },
      yAxis: {
        title: {
          text: "$(Pesos)"
        }
      },
      plotOptions: {
        line: {
          dataLabels: {
            enabled: true
          },
          enableMouseTracking: false
        }
      },
      series: this.series
    });
  }
}

customElements.define("chart-bcra", LdChart);
