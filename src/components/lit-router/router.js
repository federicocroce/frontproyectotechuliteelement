const _routes = new Set([]);

class navigate {
  static get routes() {
    return _routes;
  }

  static set routes(path) {
    _routes.add(path);
  }

  static dispatchLocation = href => {

    const path = window.location.hash.replace("#", "")

    const fullPath = navigate.routes.has(path)
      ? href
      : "*";

    document.dispatchEvent(
      new CustomEvent("set-view", {
        detail: {
          fullPath,
          path
        },
        bubbles: true,
        composed: true
      })
    );
  };

  static to = path => window.location.assign(location.origin + "#" + path);

  static reload = path => {
    if (path) {
      navigate.to(path);
    } else {
      setTimeout(() => navigate.dispatchLocation(window.location.href), 0);
    }
  };
}

export default navigate;

// window.addEventListener('hashchange', () => navigate.dispatchLocation(window.location.href), false)

window.onhashchange = function() {
  navigate.dispatchLocation(window.location.href);
};
