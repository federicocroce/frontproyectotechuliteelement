import { LitElement, html, css } from "lit-element";

class LitTransition extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        opacity: 0;
        transition: all 0.5s ease-in-out;
        transform:translateY(20px)
      }
      /* 
      section{
          opacity: 0;
      } */

      :host(.transition) {
        opacity: 1;
        transform:translateX(0)
      }
    `;
  }

  static get properties() {
    return {
      section: { type: Function }
    };
  }

  constructor() {
    super();
  }

  firstUpdated() {
    setTimeout(() => {
        this.classList.add("transition");
    }, 1);
  }

  render() {
    return html`
      <slot></slot>
    `;
  }
}

customElements.define("lit-transition", LitTransition);
