import { LitElement, html, css } from "lit-element";
import navigate from "./router";

class LitRoute extends LitElement {
  static get properties() {
    return {
      name: { type: String },
      path: { type: String },
      component: { type: Function },
      _currentPath: { type: String },
      _active: { type: Boolean },
      _matchPath: { type: Array }
    };
  }

  constructor() {
    super();
    this._currentPath = "/main";

    document.addEventListener("set-view", event => {
      event.preventDefault();
      this._currentPath = event.detail.path;
      this.setActive();
      event.stopPropagation();
    });
  }

  connectedCallback() {
    super.connectedCallback();
    this.setActive();
    navigate.routes = this.path;
  }

  setActive() {
    this._active = this._currentPath.includes(this.path);
  }

  r = () =>
    html`
      <div><slot></slot></div>
    `;
  // r = () => html`<slot></slot>`

  render() {
   if(!this._active) return;
    return html`
      <!-- ${this._active ? this.r() : null} -->
      <p>${this._active.toString()}</p>
      <slot></slot>
    `;
  }
}

customElements.define("lit-route", LitRoute);

