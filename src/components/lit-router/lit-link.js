import { LitElement, html, css } from "lit-element";

import navigate from "./router";

class Link extends LitElement {
  static get properties() {
    return {
      name: { type: String },
      path: { type: String },
      label: { type: String },
      customStyle: {},
      _selected: { type: Boolean }
    };
  }

  constructor() {
    super();
    this._selected = false;
    this.customStyle = "";
    console.log("constructor")
  }

  firstUpdated() {
    document.addEventListener("set-view", event => {
      setTimeout(() => {
        event.preventDefault();
        this._selected = this.path == event.detail.path;
        this.requestUpdate();
        console.log(`${this.path} : ${this._selected}`)
        event.stopPropagation();
      }, 100);
      this.requestUpdate();
    });
    
  }

  render() {
    return html`
      <style>
        lit-link {
          text-transform: uppercase;
          flex: 1 1 auto;
        }
        a:hover {
          cursor: pointer;
          color: gray;
          /* border-bottom: 1px solid gray; */
        }

        li {
          list-style-type: none;
        }

        a {
          color: #2d2d2d;
          font-weight: bold;
          font-family: var(--paper-font-common-base_-_font-family);
          transition: all 0.2s ease-in-out;
        }
        .underbar {
          width: 0;
          height: 2px;
          margin-top: 5px;
          background: rgba(100, 100, 200, 0);
          -webkit-transition: 0.5s ease 0.1s;
        }

        .active .underbar {
          width: 100%;
          background: #2d2d2d;
        }
      </style>

      <li class="${this._selected && "active"}" @click="${this.setView}">
        <a>${this.label}</a>
        <div class="underbar"></div>
      </li>
    `;
  }

  createRenderRoot() {
    return this;
  }

  setView() {
    this._selected = true;
    navigate.to(this.path);
  }
}

customElements.define("lit-link", Link);
