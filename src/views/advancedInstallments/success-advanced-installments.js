import { LitElement, html, css } from "lit-element";
import moment from "moment";

import { connect } from "pwa-helpers/connect-mixin.js";
import { store } from "../../config/store";
import { reducerMortgageLoans } from "../../reducers/reducerMortgageLoans";
import { reducerAdvancedInstallments } from "../../reducers/reducerAdvancedInstallments";

import { setAdvancedInstallmentsSeleted } from "../../actions/actionAdvancedInstallments";

store.addReducers({
  reducerMortgageLoans,
  reducerAdvancedInstallments
});

import "../../components/input-month-year/input-month-year";
import "../calculator-uva/load-data-section";
import "../calculator-uva/result-calculator";

import { setInputs, setFormState, clearForm } from "../../actions/actionCalculator";

import "@polymer/paper-button/paper-button.js";
import "@polymer/paper-card/paper-card.js";

import "@polymer/paper-input/paper-input.js";

class SuccessAdvancedInstallments extends connect(store)(LitElement) {
  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
      _dataLoans: { type: Object },
      _mortgageSelected: { type: Object },
      _advancedInstallments: { type: Object }
    };
  }

  constructor() {
    super();
  }

  render() {
    return html`
      ${this.currentSuccessComponent()}
    `;
  }

  currentSuccessComponent = () => {
    if (!this._advancedInstallments) return;

    if (this._advancedInstallments.length != 0) {
      const element = this._advancedInstallments[this._advancedInstallments.length - 1];
      this.setElementSelected(element);
      setFormState({
        inputs: element,
        formState: "update"
      });
      this.serviceResponse = false;
    } else {
      this.setElementSelected(
        undefined
      );
      clearForm();
    }

    return html`
      <style>
        .main-calculator-container {
          display: flex;
          min-height: 330px;
        }

        @media (max-width: 1000px) {
          .main-calculator-container {
            flex-direction: column;
          }
        }
        @media (max-width: 500px) {
          .selectors {
            flex-direction: column;
          }

          paper-card {
            margin-bottom: 15px;
            width: 100%;
          }
        }

        .card-content {
          display: flex;
          flex-direction: column;
          text-align: center;
          min-width: 150px;
          padding: 10px;
          background: #264167;
        }

        .card-content p {
          margin: 5px;
          font-weight: bold;
          font-size: 16pxpx;
          color: #c9c9c9;
          text-transform: uppercase;
        }

        .card-content p.percent {
          font-size: 23px;
          font-weight: 500;
          color: #00c6ca;
        }

        paper-input,
        input-month-year {
          flex-basis: 49%;
        }

        .selectors {
          display: flex;
          justify-content: space-evenly;
          flex-wrap: wrap;
        }

        paper-card.advanced-installments {
          width: 100%;
          padding: 30px;
        }

        paper-card.advanced-installments-item:hover {
          cursor: pointer;
          box-shadow: 0 0 20px gray;
        }

        paper-input.advanced-installments-item:first-child {
          flex-basis: 100%;
        }
      </style>

      ${this._advancedInstallments && this._advancedInstallments.length > 0
        ? html`
            <paper-card class="advanced-installments">
              <div class="selectors">
                ${this._advancedInstallments &&
          this._advancedInstallments.map(ele => {
            return html`
                      <lit-transition duration="2">
                        <paper-card
                          class="advanced-installments-item"
                          @click="${() => this.setElementSelected(ele)}"
                        >
                          <div class="card-content">
                            <p>${ele.alias}</p>
                            <p>${ele.advancedDate}</p>
                            <p class="percent">${this.setPercent(ele)} %</p>
                          </div>
                        </paper-card>
                      </lit-transition>
                    `;
          })}
              </div>
            </paper-card>
          `
        : null}
    `;
  };

  setElementSelected = element => {
    setInputs(element);
    setAdvancedInstallmentsSeleted(element);
  };

  setPercent({ currentInstallments }) {
    const debtInstallment =
      parseInt(currentInstallments) -
      parseInt(this.setCurrentInstallment(this._mortgageSelected.beginDate));

    return (
      100 -
      (debtInstallment * 100) / this._mortgageSelected.initialInstallments
    ).toFixed(2);
  }

  setCurrentInstallment(beginDate) {
    {
      const newBeginDate = moment(beginDate);
      const today = moment(new Date());
      return today.diff(newBeginDate, "month");
    }
  }

  stateChanged(state) {
    this._mortgageSelected = state.reducerMortgageLoans.mortgageSelected;
    this._advancedInstallments = state.reducerAdvancedInstallments.list;
  }
}

customElements.define(
  "success-advanced-installments",
  SuccessAdvancedInstallments
);
