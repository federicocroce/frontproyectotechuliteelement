import { LitElement, html, css } from "lit-element";

import advancedInstallments from "../../services/controllers/advancedInstallments";

import "./success-advanced-installments"


class AdvancedInstallmentsContainer extends LitElement {

  static get styles() {
    return css`
      :host {
        display: flex;
      }
      h1 {
        color: blue;
      }

      .main-calculator-container {
        display: flex;
      }

      .container {
        width: 100%;
        background: white;
      }

    `;
  }

  static get properties() {
    return {
      currentService: { type: Function }
    };
  }

  constructor() {
    super();
    this.getData();

  }

  getData = () => {
    this.currentService = advancedInstallments.getDocument();
  };

  render() {
    return html`
      <manage-services-call
        name="servicio"
        .service="${this.currentService}"
        
        .errorComponent="${({ message }) =>
          html`
            <p>${message}</p>
          `}"
        .successComponent=${() =>
          html`
            <success-advanced-installments></success-advanced-installments>
          `}
      >
      </manage-services-call>
    `;
  }

}

customElements.define("advanced-installments-container", AdvancedInstallmentsContainer);
