import { LitElement, html, css } from "lit-element";

import "../components/lit-router/lit-route";
import navigate from "../components/lit-router/router";

import { validateForm } from "../config/form";

import "../components/manage-services-call";

import "@polymer/iron-icon/iron-icon.js";
import "@polymer/iron-icons/iron-icons.js";

import loginServices from "../services/controllers/loginController";

import "@polymer/paper-button/paper-button.js";
import "@polymer/paper-toast/paper-toast.js";
import "@polymer/paper-input/paper-input.js";
import "@polymer/paper-spinner/paper-spinner.js";

import { clearData } from "../actions/actionGenerics";

class AppLogin extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        height: auto;
        min-height: 100vh;
        background: #264167;
      }
    `;
  }

  static get properties() {
    return {
      _inputs: { type: Object },
      _formSumbited: { type: Boolean },
      _toastMessage: { type: String },
      currentService: { type: Function },
      currentSuccessComponent: { type: Function }
    };
  }

  constructor() {
    super();

    this._inputs = {
      email: "",
      password: ""
    };

    this._formSumbited = false;

    sessionStorage.removeItem("token");
    clearData();
  }

  connectedCallback() {
    super.connectedCallback();
  }

  render() {
    return html`
      <style>
        .wrapper {
          max-width: 460px;
          margin: 0 auto;
          padding: 2% 5%;
        }

        @media (max-width: 1000px) {
          .wrapper {
            padding: 5%;
          }
        }

        form {
          margin-bottom: 80px;
        }

        paper-input {
          color: #c9c9c9;
          width: 100%;
        }

        paper-input {
          --paper-input-container: {
            padding: 0;
          }
          --paper-input-container-input: {
            color: #c9c9c9;
            font-family: var(--paper-font-common-base_-_font-family);
          }

          --paper-input-container-label: {
            color: #c9c9c9;
            font-family: var(--paper-font-common-base_-_font-family);
          }
          --paper-input-container-label-floating: {
            color: #c9c9c9;
            font-family: var(--paper-font-common-base_-_font-family);
          }
          --paper-input-container-label-focus: {
            color: #c9c9c9;
            font-family: var(--paper-font-common-base_-_font-family);
          }
        }

        paper-button.signin {
          --paper-button: {
            background: #157f90;
            border-radius: 2px;
            padding: 15px 0;
            width: 100%;
            color: white;
            font-family: var(--paper-font-common-base_-_font-family);
          }
        }

        paper-button.signup, paper-button.statistics {
          --paper-button: {
            padding: 20px 0;
            width: 100%;
            color: white;
            font-family: var(--paper-font-common-base_-_font-family);
          }
        }
        paper-button.statistics {
          margin-top: 10px;
          font-size:12px;
        }

        img {
          margin: 0 auto;
          display: flex;
          /* padding: 20px 0; */
          color: #157f90;
        }

        h1 {
          margin-top: 0;
          text-align: center;
          font-size: 3em;
          color: #c9c9c9;
          font-family: var(--paper-font-common-base_-_font-family);
        }

        iron-icon {
          padding: 5px 5px 5px 0;
        }
      </style>

      <div class="wrapper">
        <span class="icon-main-icon"></span>
        <img src="../../assets/creditLife.svg" />
        <!-- <svg src="src/assets/signo-de-dolar-bajo-el-techo.svg"></svg> -->
        <h1>CreditLife</h1>
        <form>
          <paper-input
            name="email"
            type="email"
            label="Mail"
            value="${this._inputs.email}"
            @input="${this.inputChange}"
            required
            ?auto-validate="${this._formSumbited}"
            error-message="Este campo es obligatorio y debe ser un mail válido"
          >
            <iron-icon icon="mail" slot="prefix"></iron-icon>
            <paper-icon-button
              slot="suffix"
              icon="clear"
              alt="clear"
              title="clear"
            >
            </paper-icon-button>
          </paper-input>

          <paper-input
            name="password"
            label="Contraseña"
            value="${this._inputs.password}"
            @input="${this.inputChange}"
            type="password"
            required
            ?auto-validate="${this._formSumbited}"
            error-message="Este campo es obligatorio"
          >
            <iron-icon icon="lock" slot="prefix"></iron-icon>
          </paper-input>
        </form>

        <paper-button class="signin" raised @click="${this.signIn}"
          >Acceder</paper-button
        >
        <paper-button class="signup" @click="${this.signUp}"
          >Crear Usuario</paper-button
        >

        <paper-button class="statistics" @click="${() => navigate.to("/main/statistics")}"
          >Cotizaciones y Estadísticas</paper-button
        >

        <manage-services-call
          name="servicio"
          .service="${this.currentService}"
          .callbackError="${({ message }) => this._openToast(message)}"
          .callbackSuccess="${response => {
            this._openToast(response.result.message);
          }}"
          .successComponent=${this.currentSuccessComponent}
        ></manage-services-call>
      </div>

      <paper-toast id="toast" duration="6000" text="${this._toastMessage}">
      </paper-toast>
    `;
  }

  inputChange(e) {
    this._inputs[e.target.name] = e.target.value;
  }

  setView(e) {
    alert(e);
  }

  cbk(param) {
    alert("CBK desde el view" + param);
  }

  _openToast(toastMessage) {
    this._toastMessage = toastMessage;
    this.shadowRoot.querySelector(`#toast`).open();
  }

  signUp() {
    this._formSumbited = true;

    if (!validateForm(this, this._inputs, true)) return;
    this.currentService = loginServices.signUp(this._inputs);
    this.currentSuccessComponent = null;
  }

  signIn() {
    this._formSumbited = true;

    if (!validateForm(this, this._inputs, true)) return;

    this.currentService = loginServices.signIn(this._inputs);

    this.currentSuccessComponent = response => {
      if (response && response.result.token) {
        sessionStorage.setItem("token", response.result.token);
        sessionStorage.setItem("email", this._inputs.email);

        this.currentSuccessComponent = response => {
          if (response) {
            navigate.to("/main");
            return html`
              <h1>${response.result.message}</h1>
            `;
          }
        };

        return html`
          <h1>${response.result.message}</h1>
        `;
      }
    };
  }
}

customElements.define("app-login", AppLogin);
