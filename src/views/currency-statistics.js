import { LitElement, html, css } from "lit-element";

import moment from "moment";

import "../components/charts/chart-bcra";
import "../components/key-value/key-value";

import bcraServices from "../services/controllers/bcraController";
import "../components/lit-transition";

import "@polymer/paper-card/paper-card.js";
import "@polymer/paper-input/paper-input.js";

import "@polymer/paper-radio-button/paper-radio-button.js";
import "@polymer/paper-radio-group/paper-radio-group.js";

class CurrencyStatistics extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
        padding: 0 3% 3% 3%;
      }

      paper-card {
        display: flex;
        padding: 2% 3%;
      }

      paper-card > * {
        flex: 1 1 auto;
      }

      paper-input {
        max-width: 400px;
        padding-right: 100px;
      }

      paper-radio-group {
        display: flex;
        vertical-align: middle;
        justify-content: space-around;
      }

      paper-radio-button {
        display: flex;
        align-items: center;
      }

      @media (max-width: 700px) {
        paper-card {
          flex-direction: column;
        }
        paper-input {
          max-width: 100%;
          padding-right: 0;
        }
      }

      @media (max-width: 500px) {
        paper-radio-group {
          flex-wrap: wrap;
        }

        paper-radio-group paper-radio-button {
          width: 40%;
        }
      }
    `;
  }

  static get properties() {
    return {
      currentService: { type: Function },
      currentSuccessComponent: { type: Function },
      average: { type: Object },
      typeChart: { type: String }
    };
  }

  constructor() {
    super();

    this.currentService = [
      bcraServices.getDocument({ route: "uva" }),
      bcraServices.getDocument({ route: "usd" })
    ];

    this.average = {};
    this.typeChart = "line";

    this.radioPotions = [
      {
        label: "LINE",
        value: "line"
      },
      {
        label: "BAR",
        value: "bar"
      },
      {
        label: "COLUMN",
        value: "column"
      },
      {
        label: "SPLINE",
        value: "spline"
      }
    ];
  }

  render() {
    return html`
      <div>
        <lit-transition>
          <paper-card>
            <paper-input
              @change="${e => {
                if (!e.target.value) return;
                this.currentService = [
                  bcraServices.getDocument({
                    route: "uva",
                    date: e.target.value
                  }),
                  bcraServices.getDocument({
                    route: "usd",
                    date: e.target.value
                  })
                ];
              }}"
              type="date"
            ></paper-input>

            <paper-radio-group selected="line">
              ${this.radioPotions.map(
                radiobutton =>
                  html`
                    <paper-radio-button
                      @click="${() => {
                        this.typeChart = radiobutton.value;
                      }}"
                      name="${radiobutton.value}"
                      >${radiobutton.label}</paper-radio-button
                    >
                  `
              )}
            </paper-radio-group>
          </paper-card>
        </lit-transition>
        <manage-services-call
          name="servicio"
          .service="${this.currentService}"
          .errorComponent="${({ message }) =>
            html`
              <p>${message}</p>
            `}"
          .successComponent="${response => {
            if (response) return this.formatChartData(response);
          }}"
        ></manage-services-call>
      </div>
    `;
  }

  formatChartData(response) {
    let series = [];
    let categories = [];
    const currentMonth = response[0].data.result.month;
    const lastUpdate = moment(
      new Date(response[0].data.result.lastUpdate)
    ).format("MM/DD/YYYY  HH:mm");

    response.map(({ data }, indexCurrency) => {
      Object.assign(this.average, {
        [data.result.currencyName]: response[indexCurrency].data.result.average
      });

      series.push({
        name: data.result.currencyName,
        data: []
      });

      Object.values(data.result.history).map((ele, index) => {
        if (indexCurrency == 0) categories.push(ele.d);
        series[indexCurrency].data.push(ele.v);
      });
    });
    return html`
      <style>
        .average-container {
          display: flex;
          justify-content: space-between;
          margin: 40px 0;
        }

        key-value .key {
          font-size: 16px;
        }
        key-value .value {
          font-size: 26px;
          display: block;
          text-align: center;
        }

        paper-card {
          width: 100%;
          padding: 5%;
        }

        .average-container > lit-transition {
          width: 45%;
        }

        .average-container paper-card {
          padding: 20px 70px;
          text-align: center;
        }

        .last-update {
          font-size: 12px;
          color: gray;
        }

        @media (max-width: 700px) {
          .selectors {
            margin-top: 8%;
          }
          .average-container {
            flex-direction: column;
          }
          .average-container > lit-transition {
            width: 100%;
            margin-bottom: 20px;
          }
        }
      </style>

      <lit-transition>
        <div class="average-container">
          ${Object.keys(this.average).map(key => {
            return html`
              <lit-transition>
                <paper-card>
                  <key-value
                    key="Promedio de ${key}"
                    value="$${this.average[key] || 0}"
                  ></key-value>
                  <span class="last-update">${lastUpdate}</span>
                </paper-card>
              </lit-transition>
            `;
          })}
        </div>

        <paper-card>
          <chart-bcra
            title="Cotizaciones de ${currentMonth}"
            .series="${series}"
            .categories="${[...categories]}"
            .typeChart="${this.typeChart}"
          ></chart-bcra>
        </paper-card>
      </lit-transition>
    `;
  }
  // createRenderRoot() {
  //   return this;
  // }
}

customElements.define("currency-statistics", CurrencyStatistics);
