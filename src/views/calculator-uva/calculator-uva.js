import { LitElement, html, css } from "lit-element";

import "../../styles/index.css";

import "../advancedInstallments/advanced-installments-container";

import { store } from "../../config/store";
import { reducerMortgageLoans } from "../../reducers/reducerMortgageLoans";

store.addReducers({
  reducerMortgageLoans
});

import "../../components/input-month-year/input-month-year";
import mortgageLoans from "../../services/controllers/mortgageLoans";
import "./load-data-section";
import "./result-calculator";

import "./success-component-calculator";

import "@polymer/paper-button/paper-button.js";
import "@polymer/paper-card/paper-card.js";

import "@polymer/paper-input/paper-input.js";
import { timeOut } from "@polymer/polymer/lib/utils/async";

class CalculatorUva extends LitElement {
  static get styles() {
    return css`
      :host{
        display: flex;
        padding: 0 3% 3% 3%;
      }
      /* 
      section {
        opacity: 0;
        transition: opacity 1s ease-in-out;
      } */
      h1 {
        color: blue;
      }

      .main-calculator-container {
        display: flex;
      }

      .container {
        width: 100%;
        background: white;
      }
    `;
  }

  static get properties() {
    return {
      currentService: { type: Function },
      _dataLoans: { type: Object }
    };
  }

  constructor() {
    super();
    this.getData();
  }


  getData = () => {
    this.currentService = mortgageLoans.getDocument();
  };

  render() {
    return html`
      <!-- <section class="section"> -->
        <manage-services-call
          name="servicio"
          .service="${this.currentService}"
          .errorComponent="${(data, error) =>
            html`
              <p>${data.message}</p>
            `}"
          .successComponent=${() =>
            html`
              <success-component-calculator></success-component-calculator>
            `}
        >
        </manage-services-call>
      <!-- </section> -->
    `;
  }
}

customElements.define("calculator-uva", CalculatorUva);
