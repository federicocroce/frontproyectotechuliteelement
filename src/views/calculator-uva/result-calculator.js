import { LitElement, html, css } from "lit-element";
import moment from "moment";

import "@polymer/paper-button/paper-button.js";
import "@polymer/paper-card/paper-card.js";

import "./chart-result";

import "../../components/key-value/key-value";

import { connect } from "pwa-helpers/connect-mixin.js";
import { store } from "../../config/store";

import { reducerCalculator } from "../../reducers/reducerCalculator";
import { reducerAdvancedInstallments } from "../../reducers/reducerAdvancedInstallments";
import { reducerMortgageLoans } from "../../reducers/reducerMortgageLoans";

store.addReducers({
  reducerCalculator,
  reducerMortgageLoans,
  reducerAdvancedInstallments
});

class ResultCalculator extends connect(store)(LitElement) {
  static get styles() {
    return css`
      :host {
        height: 100%;
        height: 1px;
        vertical-align: middle;
      }
      .container {
        width: 100%;
        display: flex;
        height: 100%;
      }

      .key-value-container {
        display: flex;
        text-align: left;
        flex: 1 1 auto;
        align-items: center;
        flex-wrap: wrap;
        flex-flow: column;
        align-items: normal;
      }

      key-value {
        display: flex;
        flex: 1 1 auto;
        flex-direction: column;
      }

      @media (max-width: 710px) {
        .key-value-container {
          flex-flow: column wrap;
          text-align: center;
        }
        key-value {
          width: auto;
          text-align: center;
          padding: 10px 0;
        }
      }

      paper-card {
        padding: 5%;
        height: 100%;
        width: 100%;
      }

      @media (max-width: 500px) {
        .container {
          flex-flow: column wrap;
        }

        .key-value-container {
          margin-top: 20px;
        }
      }
    `;
  }

  static get properties() {
    return {
      _vaules: { type: Object },
      _percent: { type: Number },
      _advancedInstallmentsSelected: { type: Object }
    };
  }

  constructor() {
    super();

    this._mortgageLoansSlected = {};
    this._values = {};
  }

  clearFormData = () => {
    this.form = {
      inputs: {},
      formState: "new",
      elementSelected: null
    };
    this.requestUpdate();
  };

  saveData = () => {
    this.currentService =
      this.form.formState == "new"
        ? mortgageLoans.post(this_inputs)
        : mortgageLoans.put(this.form.elementSelected._id, this_inputs);
  };

  render() {
    return html`
          <paper-card>
            <div class="container">
              ${html`
                <chart-result .percent=${this.setPercent()}></chart-result>
              `}

              <div class="key-value-container">
                <key-value
                  key="SU CRÉDITO INICIO HACE"
                  value=${this.setValue(this._values.currentInstallment)}
                ></key-value>
                <key-value
                  key="ADELANTÓ PAGOS POR"
                  value=${this.setValue(this._values.advancedInstallment)}
                ></key-value>

                <key-value
                  key="EN PROMEDIO ADELANTÓ POR MES"
                  value=${this.setAverage()}
                ></key-value>

                <key-value
                  key="RESTAN POR PAGAR"
                  value=${this.setValue(this._values.debtInstallment)}
                ></key-value>

                <key-value
                  key="FECHA ESTIMADA DE FINALIZACIÓN"
                  value=${this._endDate()}
                ></key-value>
              </div>
            </div>
          </paper-card>
    `;
  }

  _endDate = () => {
    var currentDate = moment(new Date());
    var futureMonth = moment(currentDate).add(
      this._values.debtInstallment,
      "M"
    );
    var futureMonthEnd = moment(futureMonth).endOf("month");

    if (
      currentDate.date() != futureMonth.date() &&
      futureMonth.isSame(futureMonthEnd.format("YYYY-MM-DD"))
    ) {
      futureMonth = futureMonth.add(this._values.debtInstallment, "d");
    }

    return futureMonthEnd.format("YYYY-MM-DD");
  };

  setValue = (value = "") => {
    return `${value} meses ${this._setYear(value)}`;
  };

  setAverage() {
    const average = (
      this._values.advancedInstallment / this._values.currentInstallment
    ).toFixed(2);
    return `${average <= 0 || !average ? 0 : average} meses`;
  }

  setPercent() {
    const percent = (
      100 -
      (this._values.debtInstallment * 100) /
      parseInt(this._mortgageLoansSlected.initialInstallments)
    ).toFixed(2);

    return percent <= 0 || !percent ? 0 : percent > 100 ? 100 : percent;
  }

  _setYear(value) {
    if (!value) return "";
    const year = Math.round((parseInt(value) / 12) * 100) / 100;
    return year >= 1 ? ` -  ${year} ${year == 1 ? "año" : "años"}` || "" : "";
  }

  setCurrentInstallment() {
    {
      const beginDate = moment(this._mortgageLoansSlected.beginDate);
      const today = moment(new Date());
      this._values.currentInstallment = today.diff(beginDate, "month");
    }
  }

  setAdvancedInstallment() {
    const advancedInstallment =
      parseInt(this._mortgageLoansSlected.initialInstallments || 0) -
      parseInt(
        this._advancedInstallmentsSelected
          ? this._advancedInstallmentsSelected.currentInstallments
          : this._mortgageLoansSlected.initialInstallments
      );

    this._values.advancedInstallment = !advancedInstallment
      ? 0
      : advancedInstallment;
  }

  setDebtInstallment() {
    this._values.debtInstallment =
      parseInt(
        this._advancedInstallmentsSelected &&
          this._advancedInstallmentsSelected.currentInstallments
          ? this._advancedInstallmentsSelected.currentInstallments
          : this._mortgageLoansSlected.initialInstallments
      ) - parseInt(this._values.currentInstallment);
  }

  updateInputData = () => {
    this.setCurrentInstallment();
    this.setAdvancedInstallment();
    this.setDebtInstallment();
    this.requestUpdate();
  };

  stateChanged(state) {
    this._advancedInstallmentsSelected = state.reducerCalculator.inputs;
    this._mortgageLoansSlected = state.reducerMortgageLoans.mortgageSelected;

    this.updateInputData();
  }
}

customElements.define("result-calculator", ResultCalculator);
