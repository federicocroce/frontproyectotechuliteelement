import { LitElement, html, css } from "lit-element";
import moment from "moment";

import "../advancedInstallments/advanced-installments-container";

import { connect } from "pwa-helpers/connect-mixin.js";
import { store } from "../../config/store";
import { reducerMortgageLoans } from "../../reducers/reducerMortgageLoans";
import { reducerAdvancedInstallments } from "../../reducers/reducerAdvancedInstallments";
import { setMortgageLoansSeleted } from "../../actions/actionMortgageLoans";

store.addReducers({
  reducerMortgageLoans,
  reducerAdvancedInstallments
});

import navigate from "../../components/lit-router/router";

import "../../components/lit-transition"


import "../../components/input-month-year/input-month-year";
import "./load-data-section";
import "./result-calculator";
import "./result-mortgage-loans";

import { setInputs, setFormState } from "../../actions/actionCalculator";

import "@polymer/paper-button/paper-button.js";
import "@polymer/paper-card/paper-card.js";

import "@polymer/paper-input/paper-input.js";

class SuccessComponentCalculator extends connect(store)(LitElement) {
  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
      _mortgageLoans: {
        type: Object
      },
      _advancedInstallments: {
        type: Object
      }
    };
  }

  constructor() {
    super();
  }

  render() {
    return html`
      ${this.currentSuccessComponent()}
    `;
  }

  currentSuccessComponent = () => {
    if (!this._mortgageLoans.list) return;

    const dataLoans = this._mortgageLoans.list;

    if (dataLoans.length != 0) {
      setMortgageLoansSeleted(dataLoans[dataLoans.length - 1]);
      this.serviceResponse = false;
    } else {
      navigate.to("/main/newMortgage");
      return;
    }

    return html`
      <style>
        .main-calculator-container {
          display: table;
          width: 100%;
          margin-bottom: 30px;
        }

        .main-calculator-container > * {
          display: table-cell;
        }

        @media (max-width: 1000px) {
          .main-calculator-container {
            display: flex;
            flex-direction: column;
          }

          .main-calculator-container > * {
            display: inline-block;
            height: 100%;
            width: 100%;
          }

          .selectors {
            margin-top: 8%;
          }
        }

        .card-content {
          display: flex;
          flex-direction: column;
          text-align: center;
          min-width: 150px;
          padding: 10px;
          background: #264167;
        }

        .card-content p {
          margin: 5px;
          font-weight: bold;
          font-size: 26px;
          color: #157f90;
          text-transform: uppercase;
        }
        .card-content p:first-of-type {
          color: #c9c9c9;
          font-size: 16px;
        }

        paper-input,
        input-month-year {
          flex-basis: 49%;
        }

        .selectors {
          display: flex;
          justify-content: space-evenly;
          flex-wrap: wrap;
        }

        paper-card {
          margin: 1%;
          height: 100%;
        }

        paper-card:hover {
          cursor: pointer;
          box-shadow: 0 0 20px gray;
        }

        paper-input:first-child {
          flex-basis: 100%;
        }
      </style>

      <result-mortgage-loans></result-mortgage-loans>

      <div class="main-calculator-container">
        <!-- <lit-transition> -->
          <load-data-section></load-data-section>
        <!-- </lit-transition> -->
        <!-- <lit-transition> -->
          <result-calculator></result-calculator>
        <!-- </lit-transition> -->
      </div>

      ${this._mortgageLoans.mortgageSelected._id &&
        html`
          <lit-transition>
            <advanced-installments-container></advanced-installments-container>
          </lit-transition>
        `}
    `;
  };

  setElementSelected = element => {
    setInputs(element.data);
    setFormState({
      inputs: element.data,
      formState: "update",
      elementSelected: element
    });
  };

  setPercent({ initialInstallments, currentInstallments, beginDate }) {
    const debtInstallment =
      parseInt(currentInstallments) -
      parseInt(this.setCurrentInstallment(beginDate));

    return (100 - (debtInstallment * 100) / initialInstallments).toFixed(2);
  }

  setCurrentInstallment(beginDate) {
    {
      const newBeginDate = moment(beginDate);
      const today = moment(new Date());
      return today.diff(newBeginDate, "month");
    }
  }

  stateChanged(state) {
    this._mortgageLoans = state.reducerMortgageLoans;
  }

}

customElements.define(
  "success-component-calculator",
  SuccessComponentCalculator
);
