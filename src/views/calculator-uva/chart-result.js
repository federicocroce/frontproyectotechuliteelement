import { LitElement, html, css } from "lit-element";

class ChartResult extends LitElement {
  static get styles() {
    return css`
      :host {
        padding: 3%;
        flex: 1 1 auto;
        display: flex;
      }
    `;
  }

  static get properties() {
    return {
      percent: { type: Number },
      _r: { type: Number },
      _strokeDasharray: { type: Number },
      _widthHeigth: { type: Number }
    };
  }

  constructor() {
    super();
    this.percent = 0;

    this._r = 90;
    this._widthHeigth = (this._r * 200) / 90;
    this._strokeDasharray = 2 * 3.1416 * this._r;
  }

  updated(changedProperties) {
    changedProperties.forEach((oldValue, propName) => {
      propName == "percent" &&
        this.percent &&
        oldValue !== this.percent &&
        setTimeout(() => {
          this._enableChart(this.percent);
        }, 0);
    });
  }

  firstUpdated(changedProperties) {
    this._enableChart(0);
  }

  render() {
    return html`
      <style>
        #svg circle {
          stroke-dashoffset: 0;
          transition: stroke-dashoffset 1s linear;
          stroke: #264167;
          stroke-width: 1.3em;
          margin: 0 auto;
        }
        #svg {
          transform: rotate(-90deg);
        }

        #svg #bar {
          stroke: #157f90;
        }
        #cont {
          display: block;
          height: 200px;
          width: 200px;
          box-shadow: 0 0 1em black;
          position: relative;
          margin: 0 auto;
          border-radius: 50%;
          box-sizing: content-box;
          position: relative;
          border-radius: 50%;
          background: transparent;
          align-self: center;
        }
        #cont:after {
          position: absolute;
          display: block;
          height: 160px;
          width: 160px;
          left: 50%;
          top: 50%;
          box-shadow: inset 0 0 1em black;
          content: attr(data-pct) "%   cancelado";
          margin-top: -80px;
          margin-left: -80px;
          border-radius: 100%;
          font-size: 1.5em;
          /* text-shadow: 0 0 0.5em black; */
          display: flex;
          text-align: center;
          align-items: center;
          font-size: 28px;
          color: #157f90;
        }
      </style>
      <div
        id="cont"
        style=${`stroke-dashoffset = ${this.percent}`}
        data-pct=${this.percent}
      >
        <svg
          id="svg"
          width="200"
          height="200"
          viewPort="0 0 100 100"
          version="1.1"
          xmlns="http://www.w3.org/2000/svg"
        >
          <circle
            r=${this._r}
            cx="100"
            cy="100"
            fill="transparent"
            stroke-dasharray=${this._strokeDasharray}
          ></circle>
          <circle
            id="bar"
            r=${this._r}
            cx="100"
            cy="100"
            fill="transparent"
            stroke-dasharray=${this._strokeDasharray}
          ></circle>
        </svg>
      </div>
    `;
  }

  _enableChart(percent) {
    var val = percent;
    var $circle = this.shadowRoot.querySelector("#svg #bar");

    if (isNaN(val)) {
      val = 100;
    } else {
      var r = this._r;
      var c = Math.PI * (r * 2);

      if (val < 0) {
        val = 0;
      }
      if (val > 100) {
        val = 100;
      }

      var pct = ((100 - val) / 100) * c;

      $circle.style.setProperty("stroke-dashoffset", pct);
    }

  }
}

customElements.define("chart-result", ChartResult);
