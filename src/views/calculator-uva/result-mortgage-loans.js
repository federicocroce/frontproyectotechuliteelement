import { LitElement, html, css } from "lit-element";

import "@polymer/paper-button/paper-button.js";
import "@polymer/paper-card/paper-card.js";

import mortgageLoans from "../../services/controllers/mortgageLoans";

import "../../components/input-month-year/input-month-year";
import "../../components/key-value/key-value";

import "@polymer/paper-input/paper-input.js";

import { connect } from "pwa-helpers/connect-mixin.js";
import { store } from "../../config/store";
import {
  setInputs,
  setFormState,
  clearForm
} from "../../actions/actionCalculator";
import { reducerCalculator } from "../../reducers/reducerCalculator";
import { reducerMortgageLoans } from "../../reducers/reducerMortgageLoans";
import { reducerAdvancedInstallments } from "../../reducers/reducerAdvancedInstallments";

store.addReducers({
  reducerCalculator,
  reducerMortgageLoans,
  reducerAdvancedInstallments
});

class ResultMortgageLoans extends connect(store)(LitElement) {
  static get styles() {
    return css`
      :host {
        display: block;
      }

      h1 {
        margin: 0;
        color: rgb(45, 45, 45);
      }

      paper-button.save {
        --paper-button: {
          background: #157f90;
          border-radius: 2px;
          padding: 15px 0;
          width: 100%;
          color: white;
        }
      }

      paper-button.clear {
        --paper-button: {
          padding: 20px 0;
          width: 100%;
          color: #157f90;
        }
      }

      .key-value-container {
        flex: 1 1 auto;
        display: flex;
        align-items: center;
        justify-content: space-evenly;
      }

      key-value .value {
        padding-left: 10px;
      }

      @media (max-width: 850px) {
        :host {
          width: auto;
          flex-direction: column;
          margin: 10px 0;
        }
        .key-value-container {
          flex-direction: column;
          align-items: initial;
        }
        key-value .value {
          padding-left: 0;
        }
        paper-card {
          flex-direction: column;
          padding: 20px;
        }

        h1{
          margin-bottom: 10px;
        }

        key-value .value {
          font-size: 15px;
        }
        .container {
          flex-direction: column;
        }

        .delete {
          align-self: flex-end;
          order: 0;
        }
      }
    `;
  }

  static get properties() {
    return {
      _inputs: { type: Object },
      _form: { type: Object },
      _mortgageSelected: { type: Object },
      _advancedInstallments: { type: Object }
    };
  }

  constructor() {
    super();

    // this._inputs = {};
    // setFormState({
    //   formState: "new",
    //   elementSelected: null
    // });
  }

  clearFormData = () => {
    setFormState({
      formState: "new",
      elementSelected: null
    });
    setInputs({ currentInstallments: "" });
    this.requestUpdate();
  };

  saveData = () => {
    const { advancedDate, alias, currentInstallments } = this._inputs;
    const payload = {
      alias,
      currentInstallments,
      advancedDate,
      _idMortgageLoans: this._mortgageSelected._id
    };

    this._form.formState == "new"
      ? advancedInstallments.post(payload)
      : advancedInstallments.put(
          this._advancedInstallments.advancedInstallmentsSelected._id,
          payload
        );
  };

  inputChange = e => {
    setInputs({ [e.target.name]: e.target.value });
  };

  render() {
    return html`
      <style>
        .container {
          display: flex;
          flex-direction: row;
          width: 100%;
          justify-content: space-between;
        }

        paper-card {
          margin: 0 0 30px 0;
          padding: 30px;
          display: flex;
          /* text-align: left;
          align-items: baseline;
          justify-content: baseline; */
        }

        .container > * {
          order: 1;
        }

        iron-icon {
          color: rgb(45, 45, 45);
          transition: all .2s ease-in-out;
        }
        iron-icon:hover {
          cursor: pointer;
          color: rgb(95, 95, 95);
        }

        .delete {
          align-self: center;
        }
      </style>

      <paper-card>
        <div class="container">
          <h1>${this._mortgageSelected.aliasMortgageLoans || ""}</h1>

          <div class="key-value-container">
            <key-value
              key="CUOTAS INICIALES"
              value=${this.setValue(this._mortgageSelected.initialInstallments)}
            ></key-value>
            <key-value
              key="FECHA INICIAL"
              value=${this._mortgageSelected.beginDate}
            ></key-value>
          </div>

          <iron-icon
            class="delete"
            @click="${() => mortgageLoans.remove(this._mortgageSelected._id)}"
            icon="delete"
          ></iron-icon>
        </div>
      </paper-card>
    `;
  }

  setValue = (value = "") => {
    return `${value} meses ${this._setYear(value)}`;
  };
  _setYear(value) {
    if (!value) return;
    const year = Math.round((parseInt(value) / 12) * 100) / 100;
    return year >= 1 ? ` -  ${year} ${year == 1 ? "año" : "años"}` || "" : "";
  }

  stateChanged(state) {
    this._mortgageSelected = state.reducerMortgageLoans.mortgageSelected;
  }
}

customElements.define("result-mortgage-loans", ResultMortgageLoans);
