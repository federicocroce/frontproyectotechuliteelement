import { LitElement, html, css } from "lit-element";

import { validateForm } from "../../config/form";

import "@polymer/paper-button/paper-button.js";
import "@polymer/paper-card/paper-card.js";

import advancedInstallments from "../../services/controllers/advancedInstallments";

import "../../components/input-month-year/input-month-year";

import "@polymer/paper-input/paper-input.js";

import { connect } from "pwa-helpers/connect-mixin.js";
import { store } from "../../config/store";
import { setInputs, setFormState } from "../../actions/actionCalculator";
import { reducerCalculator } from "../../reducers/reducerCalculator";
import { reducerMortgageLoans } from "../../reducers/reducerMortgageLoans";
import { reducerAdvancedInstallments } from "../../reducers/reducerAdvancedInstallments";

store.addReducers({
  reducerCalculator,
  reducerMortgageLoans,
  reducerAdvancedInstallments
});

class LoadDataSection extends connect(store)(LitElement) {
  static get styles() {
    return css`
      :host {
        display: block;
        flex: 1 1 auto;
        height: 100%;
        max-width: 500px;
        height: 100%;
        margin-bottom: 30px;
      }

      @media (max-width: 1000px) {
        :host {
          width: auto;
          max-width: 100%;
        }
      }

      .container {
        width: 100%;
        padding-bottom: 30px;
      }
    `;
  }

  static get properties() {
    return {
      _inputs: { type: Object },
      _form: { type: Object },
      _formSumbited: { type: Boolean },
      _mortgageSelected: { type: Object },
      _advancedInstallments: { type: Object }
    };
  }

  constructor() {
    super();
    this.clearFormData();
  }

  clearFormData = () => {
    this._formSumbited = false;
    setFormState({
      formState: "new",
      elementSelected: null
    });
    setInputs({
      alias: "",
      currentInstallments: "",
      advancedDate: ""
    });
    this.requestUpdate();
  };

  saveData = () => {
    this._formSumbited = true;
    if (!validateForm(this, this._inputs, true)) return;

    const { advancedDate, alias, currentInstallments } = this._inputs;
    const payload = {
      alias,
      currentInstallments,
      advancedDate,
      _idMortgageLoans: this._mortgageSelected._id
    };

    this._form.formState == "new"
      ? advancedInstallments.post(payload)
      : advancedInstallments.put(
          this._advancedInstallments.advancedInstallmentsSelected._id,
          payload
        );
  };

  inputChange = e => {
    setInputs({ [e.target.name]: e.target.value });
  };

  render() {
    return html`
      <style>
        paper-button.save {
          --paper-button: {
            background: #157f90;
            border-radius: 2px;
            padding: 15px 0;
            width: 100%;
            color: white;
            margin: 0;
          }
        }

        paper-button.clear {
          --paper-button: {
            padding: 20px 0;
            width: 100%;
            color: #157f90;
            margin: 0;
          }
        }

        .container-form {
          padding: 30px;
          display: flex;
          flex-direction: column;
        }

        .delete {
          align-self: flex-end;
        }

        @media (max-width: 1000px) {
          paper-card {
            width: 100%;
          }
        }

        @media (max-width: 850px) {
          .container-form {
            padding: 20px;
          }
        }

        iron-icon {
          color: rgb(45, 45, 45);
          transition: all .2s ease-in-out;
        }
        iron-icon:hover {
          cursor: pointer;
          color: rgb(95, 95, 95);
        }

        .card-actions {
          border: 0;
          padding: 0;
        }
      </style>

      <lit-transition>
        <paper-card>
          <div class="container-form">
            ${this._form.formState != "new"
              ? html`
                  <iron-icon
                    class="delete"
                    @click="${() =>
                      advancedInstallments.remove(
                        this._advancedInstallments.advancedInstallmentsSelected
                          ._id,
                        this._mortgageSelected._id
                      )}"
                    icon="delete"
                  ></iron-icon>
                `
              : null}

            <div class="container">
              <paper-input
                name="alias"
                label="ALIAS"
                type="text"
                value="${this._inputs.alias || ""}"
                @input="${this.inputChange}"
                required
                ?auto-validate="${this._formSumbited}"
                error-message="Este campo es obligatorio"
              ></paper-input>

              <input-month-year
                name="currentInstallments"
                label="CUOTAS ACTUALES"
                type="number"
                ?required="${true}"
                ?autoValidate="${this._formSumbited}"
                value="${this._inputs.currentInstallments}"
                .input="${this.inputChange}"
              >
              </input-month-year>

              <paper-input
                name="advancedDate"
                label="FECHA DEL ADELANTO"
                type="date"
                value="${this._inputs.advancedDate}"
                @change="${this.inputChange}"
                required
                ?auto-validate="${this._formSumbited}"
                error-message="Este campo es obligatorio"
              >
              </paper-input>
            </div>
            <div class="card-actions">
              <paper-button class="save" raised @click="${this.saveData}"
                >${this._form.formState == "new"
                  ? "Guardar"
                  : "Actualizar"}</paper-button
              >
              <paper-button class="clear" @click="${this.clearFormData}"
                >${this._form.formState == "new"
                  ? "Limpiar"
                  : "Nuevo adelanto"}</paper-button
              >
            </div>
          </div>
        </paper-card>
      </lit-transition>
    `;
  }

  stateChanged(state) {
    this._inputs = state.reducerCalculator.inputs;
    this._form = state.reducerCalculator.form;
    this._mortgageSelected = state.reducerMortgageLoans.mortgageSelected;
    this._advancedInstallments = state.reducerAdvancedInstallments;
  }
}

customElements.define("load-data-section", LoadDataSection);
