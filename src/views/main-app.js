import { LitElement, html, css } from "lit-element";

import navigate from "../components/lit-router/router";
import "../components/lit-router/lit-link";

class MainApp extends LitElement {
  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {};
  }

  firstUpdated() {
    sessionStorage.token && navigate.to("/main/calculator");
  }

  constructor() {
    super();
  }

  render() {
    return html`
      <style>
        .navigation-bar {
          display: flex;
          text-align: center;
          padding: 30px 0;
          margin-bottom: 10px;
        }

        .active {
          color: green;
          padding-bottom: 4px;
          transition: color 1s linear;
        }
      </style>

      <div class="navigation-bar">
        <lit-link path="/main/calculator" label="ADELANTOS"></lit-link>
        <lit-link path="/main/statistics" label="Estadísticas"></lit-link>
        <lit-link path="/login" label="${sessionStorage.token && sessionStorage.token != "undefined" ? 'LogOut' : 'login'}">LogOut</lit-link>
      </div>
    `;
  }
}

customElements.define("main-app", MainApp);
