import { LitElement, html, css } from "lit-element";

import "@polymer/paper-button/paper-button.js";
import "@polymer/paper-card/paper-card.js";

import mortgageLoans from "../../services/controllers/mortgageLoans";

import { validateForm } from "../../config/form";

import "../../components/input-month-year/input-month-year";

import "@polymer/paper-input/paper-input.js";

import navigate from "../../components/lit-router/router";

import { connect } from "pwa-helpers/connect-mixin.js";
import { store } from "../../config/store";

import { setInputs, setFormState } from "../../actions/actionCalculator";
import { clearData } from "../../actions/actionGenerics";

class NewMortgage extends connect(store)(LitElement) {
  static get styles() {
    return css`
      :host {
        display: block;
        padding: 0 5% 5% 5%;
        max-width: 800px;
        margin: 0 auto;
      }
    `;
  }

  static get properties() {
    return {
      _inputs: { type: Object },
      _form: { type: Object },
      _formSumbited: { type: Boolean }
    };
  }

  constructor() {
    super();
    this.clearFormData();
    clearData();
  }

  clearFormData = () => {
    this._formSumbited = false;
    setFormState({
      formState: "new",
      elementSelected: null
    });
    setInputs({
      aliasMortgageLoans: "",
      initialInstallments: "",
      beginDate: ""
    });
    this.requestUpdate();
  };

  saveData = () => {
    this._formSumbited = true;
    if (!validateForm(this, this._inputs, true)) return;

    const { beginDate, initialInstallments, aliasMortgageLoans } = this._inputs;
    const payload = {
      aliasMortgageLoans: aliasMortgageLoans,
      beginDate,
      initialInstallments
    };

    mortgageLoans.post(payload).then(result => navigate.to("/main/calculator"))

    // navigate.to("/main/calculator");
  };

  inputChange = e => {
    setInputs({ [e.target.name]: e.target.value });
  };

  render() {
    return html`
      <style>
        paper-input {
          --paper-input-container: {
            padding: 0;
          }
        }

        paper-card {
          width: 100%;
          padding: 20px;
        }
        .card-actions {
          border: 0;
          padding: 0;
        }

        paper-button.save {
          --paper-button: {
            background: #157f90;
            border-radius: 2px;
            padding: 15px 0;
            width: 100%;
            color: white;
          }
        }

        paper-button.clear {
          --paper-button: {
            padding: 20px 0;
            width: 100%;
            color: #157f90;
          }
        }

        .card-actions {
          margin-top: 30px;
        }

        h1 {
          text-transform: uppercase;
        }

        @media (max-width: 600px) {
          h1{
            font-size: 1.6em;
          }
        }
      </style>

      <paper-card>
        <h1>Datos iniciales del crédito</h1>
        <div class="container">
          <paper-input
            name="aliasMortgageLoans"
            label="ALIAS DEL CRÉDITO"
            type="text"
            value="${this._inputs.aliasMortgageLoans || ""}"
            @input="${this.inputChange}"
            required
            ?auto-validate="${this._formSumbited}"
            error-message="Este campo es obligatorio"
          >
          </paper-input>

          <input-month-year
            name="initialInstallments"
            label="CUOTAS INICIALES"
            type="number"
            value="${this._inputs.initialInstallments}"
            .input="${this.inputChange}"
            ?required="${true}"
            ?autoValidate="${this._formSumbited}"
          >
          </input-month-year>

          <paper-input
            name="beginDate"
            label="FECHA DE INICIO"
            type="date"
            value="${this._inputs.beginDate}"
            @change="${this.inputChange}"
            required
            ?auto-validate="${this._formSumbited}"
            error-message="Este campo es obligatorio"
          >
          </paper-input>
        </div>
        <div class="card-actions">
          <paper-button class="save" raised @click="${this.saveData}"
            >${this._form.formState == "new"
              ? "Guardar"
              : "Actualizar"}</paper-button
          >
          <paper-button class="clear" @click="${this.clearFormData}"
            >${this._form.formState == "new"
              ? "Limpiar"
              : "Nuevo adelanto"}</paper-button
          >
        </div>
      </paper-card>
    `;
  }

  stateChanged(state) {
    this._inputs = state.reducerCalculator.inputs || {
      aliasMortgageLoans: "",
      initialInstallments: "",
      beginDate: ""
    };
    this._form = state.reducerCalculator.form;
  }
}

customElements.define("new-mortgage", NewMortgage);
