import { LitElement, html, css } from "lit-element";

import { connect } from "pwa-helpers/connect-mixin.js";
import { store } from "../config/store";

import navigate from "../components/lit-router/router";
import "../components/lit-router/lit-transition";
import "../styles/index.css";

import "./main-app";
import "./app-login";
import "./calculator-uva/calculator-uva";
import "./new-mortgage/new-mortgage";
import "./currency-statistics";

class MyApp extends connect(store)(LitElement) {
  constructor() {
    super();
  }

  connectedCallback() {
    super.connectedCallback();
    navigate.reload();
  }

  render() {
    return html`
      <lit-route
        path="/main"
        .component="${() => html`
          <main-app></main-app>
        `}"
      ></lit-route>
      <lit-route
        path="/main/newMortgage"
        .component="${() =>
          html`
          <lit-transition .section="${() => html `<new-mortgage></new-mortgage>`}"></lit-transition>         
          `}"
      ></lit-route>
      <lit-route
        path="/main/calculator"
        .component="${() =>
          html`
          <lit-transition .section="${() => html `<calculator-uva></calculator-uva>`}"></lit-transition>             
          `}"
      ></lit-route>
      <lit-route
        path="/main/statistics"
        .component="${() =>
          html`
          <lit-transition .section="${() => html `<currency-statistics></currency-statistics>`}"></lit-transition>            
          `}"
      ></lit-route>
      <lit-route
        path="/login"
        .component="${() =>
          html`
            <app-login></app-login>
          `}"
      ></lit-route>
      <lit-route
        path="*"
        .component="${() =>
          html`
            <h1>Not Found</h1>
          `}"
      ></lit-route>
    `;
  }
  // createRenderRoot() {
  //   return this;
  // }
}
customElements.define("my-app", MyApp);
