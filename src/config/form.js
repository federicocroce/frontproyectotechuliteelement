export const validateForm = (context, inputs, manualValidate = false) => {
  let validForm = inputs && Object.keys(inputs).length > 0 ? true : false;

  Object.keys(inputs).map(key => {
    const input = context.shadowRoot.querySelector(`[name='${key}']`);
    if (!input) return;

    if (manualValidate) {
      if (!input.validate()) {
        validForm = false;
      }
    } else {
      if (input.invalid) {
        validForm = false;
      }
    }
  });

  return validForm;
};
