import {store} from '../config/store';

const setInputs = (values) => {
    store.dispatch({
        type: 'SET_INPUTS',
        payload: values
    });
}

const setFormState = (values) => {
    store.dispatch({
        type: 'SET_FORM_CALCULATOR',
        payload: values
    });
}

const clearForm = (values = {}) => {
    store.dispatch({
        type: 'CLEAR_FORM_CALCULATOR',
        payload: values
    });
}


export {
    setInputs,
    setFormState,
    clearForm
};
