import { initialMortgageLoans } from "../actions/actionMortgageLoans";
import { clearForm } from "../actions/actionCalculator";
import { initialAdvancedInstallments } from "../actions/actionAdvancedInstallments";

import advancedInstallments from "../services/controllers/advancedInstallments"
import mortgageLoans from "../services/controllers/mortgageLoans"

export const clearData = () => {
  initialMortgageLoans(undefined);
  clearForm(undefined);
  initialAdvancedInstallments(undefined);
  advancedInstallments && advancedInstallments.invokedSocket();
  mortgageLoans && mortgageLoans.invokedSocket();
};
