import {store} from '../config/store';

const getMortgageLoans = (values) => {
    store.dispatch({
        type: 'GET_MORTGAGE_LOANS',
        payload: values
    });
}
const setMortgageLoansSeleted = (values) => {
    store.dispatch({
        type: 'SET_MORTGAGE_LOANS_SELECTED',
        payload: values
    });
}

const initialMortgageLoans = (values) => {
    store.dispatch({
        type: 'INITIAL_MORTGAGE_LOANS',
        payload: values
    });
}

export {
    getMortgageLoans,
    setMortgageLoansSeleted,
    initialMortgageLoans
};
