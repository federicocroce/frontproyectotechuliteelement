import {store} from '../config/store';

const getAdvancedInstallments = (values) => {
    store.dispatch({
        type: 'GET_ADVANCED_INSTALLMENTS',
        payload: values
    });
}
const setAdvancedInstallmentsSeleted = (values) => {
    store.dispatch({
        type: 'SET_ADVANCED_INSTALLMENTS_SELECTED',
        payload: values
    });
}

const initialAdvancedInstallments = (values) => {
    store.dispatch({
        type: 'INITIAL_ADVANCED_INSTALLMENTS',
        payload: values
    });
}

// const testActionMort = () => console.log("test")

export {
    getAdvancedInstallments,
    setAdvancedInstallmentsSeleted,
    initialAdvancedInstallments
};
