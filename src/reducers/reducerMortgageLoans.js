const initialState = {
  list: undefined,
  mortgageSelected: undefined
};

const reducerMortgageLoans = (state = initialState, action) => {
  switch (action.type) {
    case "GET_MORTGAGE_LOANS":
      return {
        ...state,
        list: action.payload
      };
    case "SET_MORTGAGE_LOANS_SELECTED":
      return {
        ...state,
        mortgageSelected: action.payload
      };
    case "INITIAL_MORTGAGE_LOANS":
      return {
        ...state,
        list: undefined,
        mortgageSelected: undefined
      };

    default:
      return state;
  }
};

export { reducerMortgageLoans };
