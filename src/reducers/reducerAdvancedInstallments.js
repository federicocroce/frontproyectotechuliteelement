const initialState = {
  list: undefined,
  advancedInstallmentsSelected: undefined
};

const reducerAdvancedInstallments = (state = initialState, action) => {
  switch (action.type) {
    case "GET_ADVANCED_INSTALLMENTS":
      return {
        ...state,
        list: action.payload
      };
    case "SET_ADVANCED_INSTALLMENTS_SELECTED":
      return {
        ...state,
        advancedInstallmentsSelected: action.payload
      };
    case "INITIAL_ADVANCED_INSTALLMENTS":
      return {
        ...state,
        list: undefined,
        advancedInstallmentsSelected: undefined
      };

    default:
      return state;
  }
};

export { reducerAdvancedInstallments };
