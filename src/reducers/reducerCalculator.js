const initialState = {
  inputs: {},
  form: {}
};

const reducerCalculator = (state = initialState, action) => {
  switch (action.type) {
    case "SET_INPUTS":
      return {
        ...state,
        inputs: { ...state.inputs, ...action.payload }
      };

    case "SET_FORM_CALCULATOR":
      return {
        ...state,
        form: { ...state.form, ...action.payload }
      };

    case "CLEAR_FORM_CALCULATOR":
      return {
        ...state,
        inputs: {}
      };

    default:
      return state;
  }
};

export { reducerCalculator };
