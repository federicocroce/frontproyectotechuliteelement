const webpack = require("webpack");
const { resolve } = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const webpackMerge = require("webpack-merge");
const CopyWebpackPlugin = require("copy-webpack-plugin");
const CleanWebpackPlugin = require("clean-webpack-plugin");

const modeConfig = env => require(`./build-utils/webpack.${env.mode}.js`)(env);
const loadPresets = require("./build-utils/loadPresets");

const webcomponentsjs = "./node_modules/@webcomponents/webcomponentsjs";

const polyfills = [
  {
    from: resolve(`${webcomponentsjs}/webcomponents-*.{js,map}`),
    to: "vendor",
    flatten: true
  },
  {
    from: resolve(`${webcomponentsjs}/bundles/*.{js,map}`),
    to: "vendor/bundles",
    flatten: true
  },
  {
    from: resolve(`${webcomponentsjs}/custom-elements-es5-adapter.js`),
    to: "vendor",
    flatten: true
  },
  {
  from: resolve(`node_modules/@webcomponents/shadycss/apply-shim.min.js`),
  to: "node_modules/@webcomponents/shadycss/"
  }
];


const assets = [
  {
    from: "assets",
    to: "assets/"
  }
];

const server = [
  {
    from: "server",
    to: ""
  }
];

const plugins = [
  new CleanWebpackPlugin(["dist"]),
  new webpack.ProgressPlugin(),
  new HtmlWebpackPlugin({
    filename: "index.html",
    template: "./src/index.html",
    minify: {
      collapseWhitespace: true,
      minifyCSS: true,
      minifyJS: true
    }
  }),
  new CopyWebpackPlugin([...polyfills, ...assets, ...server], {
    ignore: [".DS_Store"]
  })
];

module.exports = ({ mode, presets }) => {
  return webpackMerge(
    {
      mode,
      devServer: {
        compress: true,
        host: "0.0.0.0",
        port: 3001
      },
      output: {
        filename: "[name].[chunkhash:8].js"
      },
      resolve: {
        // Add `.ts` and `.tsx` as a resolvable extension.
        extensions: [".ts", ".tsx", ".js"]
      },
      module: {
        rules: [
          {
            test: /\.tsx?$/,
            use: "ts-loader",
            exclude: /node_modules/
          },
          {
            test: /\.js$/,
            exclude: /node_modules/,
            loader: "babel-loader",
            options: {
              plugins: [
                "@babel/plugin-syntax-dynamic-import",
                "@babel/plugin-proposal-class-properties"
              ],
              presets: [
                [
                  "@babel/preset-env",
                  {
                    useBuiltIns: "usage",
                    targets: ">1%, not dead, not ie 11"
                  }
                ]
              ]
            }
          }
        ]
      },
      plugins
    },
    modeConfig({ mode, presets }),
    loadPresets({ mode, presets })
  );
};
